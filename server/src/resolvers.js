const { paginateResults } = require('./utils');

module.exports = {
  Query: {
    countries: async (_, { pageSize = 20, after }, { dataSources }) => {

      const allCountries = await dataSources.countryAPI.getAllCountries();

      const countries = paginateResults({
        after,
        pageSize,
        results: allCountries
      });

      return {
        countries,
        cursor: countries.length ? countries[countries.length-1].cursor : null,
        hasMore: countries.length
          ? countries[countries.length - 1].cursor !==
            allCountries[allCountries.length - 1].cursor
          : false
      };

    },
  }
};
