const { gql } = require('apollo-server');

const typeDefs = gql`
  # Your schema will go here

  type Query {
    countries(
      pageSize: Int
      after: String
    ): CountryConnection!
  }

  type Country {
    id: ID!
    name: String
    capital: String
    region: String
    population: Int
    flag: String
  }

  type CountryConnection {
    cursor: String!
    hasMore: Boolean!
    countries: [Country]!
  }

`;

module.exports = typeDefs;
