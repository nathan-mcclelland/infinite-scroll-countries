const { ApolloServer } = require('apollo-server');
const typeDefs = require('./schema');
const resolvers = require('./resolvers');

const CountryAPI = require('./datasources/country');

const server = new ApolloServer({
  typeDefs,
  resolvers,
  dataSources: () => ({
    countryAPI: new CountryAPI(),
  })
});

server.listen().then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});
