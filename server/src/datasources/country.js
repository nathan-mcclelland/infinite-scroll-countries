const { RESTDataSource } = require('apollo-datasource-rest');

class CountryAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = 'https://restcountries.eu/rest/v2/'
  }

  // Makes a get request to https://restcountries.eu/rest/v2/all
  async getAllCountries() {
    const response = await this.get('all')
    return Array.isArray(response)
      ? response.map(country => this.countryReducer(country))
      : [];
  }

  countryReducer(country) {
    return {
      id: country.alpha2Code,
      cursor: `${country.alpha2Code}`,
      name: country.name,
      capital: country.capital,
      population: country.population,
      region: country.region,
      flag: country.flag,
    };
  }
}

module.exports = CountryAPI
