import React, { Component } from 'react'
import './App.scss'
import CountryPanel from './components/_countryPanel'
import ApolloClient from 'apollo-boost'
import { ApolloProvider, Query } from 'react-apollo'
import gql from 'graphql-tag'

const client = new ApolloClient({
  uri: "http://localhost:4000/"
})

const COUNTRIES_QUERY = gql`
  query countiresList($after: String) {
    countries(after: $after) {
      cursor
      hasMore
      countries {
        id
        name
        flag
      }
    }
  }
`;

export class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <Query query={COUNTRIES_QUERY}>
          {
            ({ loading, data, fetchMore }) => {

              if(loading) return "Loading..."

              const { countries } = data.countries

              if(!loading) return (
                <CountryPanel
                  countries={countries}
                  hasMore={data.countries.hasMore}
                  onLoadMore={() =>
                    fetchMore({
                      variables: {
                        after: data.countries.cursor,
                      },
                      updateQuery: (prev, { fetchMoreResult, ...rest }) => {
                        if (!fetchMoreResult) return prev;
                        return {
                          ...fetchMoreResult,
                          countries: {
                            ...fetchMoreResult.countries,
                            countries: [
                              ...prev.countries.countries,
                              ...fetchMoreResult.countries.countries,
                            ],
                          },
                        };
                      },
                    })}
                />
              )

            }
          }
        </Query>
      </ApolloProvider>
    );
  }
}

export default App;
