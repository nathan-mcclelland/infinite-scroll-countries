import React from 'react';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';

// Checks if the user has scrolled to the bottom of the Flags Container and checks if there is more data to load. 
const handleScroll = ({ currentTarget }, onLoadMore, hasMore) => {
  if (
    currentTarget.scrollTop + currentTarget.clientHeight >=
    currentTarget.scrollHeight && hasMore
  ) {
    onLoadMore();
  }
};

const CountryPanel = ({ countries, hasMore, onLoadMore }) => (
	<Container onScroll={e => handleScroll(e, onLoadMore, hasMore)} className="flagContainer">
		<h1>Inifnite Scrolling Countries</h1>
	  <Grid container spacing={3}>
	    {
	      countries.map(country => (
	        <Grid item xs={12} sm={6} md={4} key={country.id}>
						<div className="countryPanel">
						  <div className="flagHolder">
						    <img src={country.flag} alt={"Flag of " + country.name} />
						  </div>
						  <div className="detailHolder">
						    <h1>{ country.name }</h1>
						  </div>
						</div>
	        </Grid>
	      ))
	    }
	  </Grid>
	</Container>
);

export default CountryPanel;
