## Introduction

This application displays flags of every recognised country from around the world. Initially the application shows the first 20 countries and their flags. Once a user scrolls to the bottom of the list of displayed flags a further 20 will load. This will continue until there are no more countries left to display.

This app implements an Apollo server to obtain the data, documentation for which can be found [here](https://www.apollographql.com/docs/).

React is then used for the frontend of the application with Material UI being used for some basic styling components such as a grid.

The API used to obtain the data can be found [here](https://restcountries.eu/rest/v2/).

## To Run the App

The app consists of two folders:

### Server

All files within the server folder focus on the set up of the Apollo Server. To start the server simply navigate to the server folder and run the following command:

### `npm i && npm start`

This should start the server on [http://localhost:4000](http://localhost:4000).

### Client

Once you have the server up and running you should then navigate to the client folder and run the following command:

### `npm i && npm start`

This will run the frontend of the application in development mode on [http://localhost:3000](http://localhost:3000).
